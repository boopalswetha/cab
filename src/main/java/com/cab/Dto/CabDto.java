package com.cab.Dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class CabDto {
	@Temporal(TemporalType.DATE)
	private Date Date;

	public Date getDate() {
		return Date;
	}

	public void setDate(Date date) {
		Date = date;
	}
	

}
