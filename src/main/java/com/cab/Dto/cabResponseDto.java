package com.cab.Dto;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class cabResponseDto {

private Long cabId;
	
	private String cabName;
	
	private String regdNo;

	private String avilableAt ;
    
    private boolean cabStatus;

	public Long getCabId() {
		return cabId;
	}

	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}

	public String getCabName() {
		return cabName;
	}

	public void setCabName(String cabName) {
		this.cabName = cabName;
	}

	public String getRegdNo() {
		return regdNo;
	}

	public void setRegdNo(String regdNo) {
		this.regdNo = regdNo;
	}

	public String getAvilableAt() {
		return avilableAt;
	}

	public void setAvilableAt(String avilableAt) {
		this.avilableAt = avilableAt;
	}

	public boolean isCabStatus() {
		return cabStatus;
	}

	public void setCabStatus(boolean cabStatus) {
		this.cabStatus = cabStatus;
	}
    
    

}
