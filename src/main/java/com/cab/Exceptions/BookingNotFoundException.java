package com.cab.Exceptions;

public class BookingNotFoundException extends RuntimeException {
	public BookingNotFoundException(String string) {
		super(string);
	}
}
