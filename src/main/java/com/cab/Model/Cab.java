package com.cab.Model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Cab {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cabId;
	
	private String cabName;
	
	private String regdNo;
		
	
    public String getRegdNo() {
		return regdNo;
	}


	public void setRegdNo(String regdNo) {
		this.regdNo = regdNo;
	}



	private LocalDateTime timeStamp ;
    
    private boolean cabStatus;

  @JsonIgnore
	@OneToMany(mappedBy = "cab", cascade = CascadeType.ALL)
	private List<Booking> bookings;


	public Long getCabId() {
		return cabId;
	}


	public void setCabId(Long cabId) {
		this.cabId = cabId;
	}


	public String getCabName() {
		return cabName;
	}


	public void setCabName(String cabName) {
		this.cabName = cabName;
	}




	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}


	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}


	public boolean isCabStatus() {
		return cabStatus;
	}


	public void setCabStatus(boolean cabStatus) {
		this.cabStatus = cabStatus;
	}


	public List<Booking> getBookings() {
		return bookings;
	}


	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	
}
