package com.cab.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cab.Model.Booking;
import com.cab.Model.User;

public interface BookingRepository extends JpaRepository<Booking, Long> {
  List<Booking>  findByUserAndBookingTimeBetween(User user,Date fromdate ,Date toDate);
  
  @Query(value = "SELECT * FROM booking WHERE  bookingTime Like %?1%" , nativeQuery = true)
  List<Booking> findByMatchMonthAndMatchDay(@Param ("eventDate") String eventDate);
  
  Booking findByUserAndBookingId(User user,Long bookingId);
  
  
  @Query(value = "SELECT * FROM booking  WHERE bookingTime Like %?1% and user_userId =?2" , nativeQuery = true)
  List<Booking> findByMatchMonthAndMathDay(@Param ("eventDate") String eventDate,@Param ("userId")Long userId);
  
}
